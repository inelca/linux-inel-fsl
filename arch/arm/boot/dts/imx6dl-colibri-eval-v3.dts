/*
 * Copyright 2014-2015 Toradex AG
 * Copyright 2012 Freescale Semiconductor, Inc.
 * Copyright 2011 Linaro Ltd.
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/dts-v1/;

#include <dt-bindings/input/input.h>
#include <dt-bindings/interrupt-controller/irq.h>
#include "imx6dl.dtsi"
#include "imx6qdl-colibri.dtsi"

/* Add the following define if you connect a Fusion display with a capacitive
   touch controller */
/* #define PCAP */

/ {
	model = "Toradex Colibri iMX6DL/S on Colibri Evaluation Board V3";
	compatible = "toradex,colibri_imx6dl-eval", "toradex,colibri_imx6dl", "fsl,imx6dl";

	aliases {
		i2c0 = &i2cddc;
		i2c1 = &i2c2;
		i2c2 = &i2c3;
	};

	aliases {
		rtc0 = &rtc_i2c;
		rtc1 = "/soc/aips-bus@02000000/snvs@020cc000/snvs-rtc-lp@34";
	};

	aliases {
		/* the following, together with kernel patches, forces a fixed assignment
		   between device id and usdhc controller */
		/* i.e. the eMMC on usdhc3 will be /dev/mmcblk0 */
		mmc0 = &usdhc3; /* eMMC */
		mmc1 = &usdhc1; /* MMC 4bit slot */
	};

	extcon_usbc_det: usbc_det {
		compatible = "linux,extcon-usb-gpio";
		debounce = <25>;
		id-gpio = <&gpio7 12 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_usbc_det_1>;
	};

	gpio-keys {
		compatible = "gpio-keys";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_gpio_keys>;

		wakeup {
			label = "Wake-Up";
			gpios = <&gpio2 22 GPIO_ACTIVE_HIGH>;
			linux,code = <KEY_WAKEUP>;
			debounce-interval = <10>;
			gpio-key,wakeup;
		};
	};

	pwmleds {
		compatible = "pwm-leds";
#ifndef PCAP
		ledpwm2 {
			label = "PWM<B>";
			pwms = <&pwm1 0 50000>;
			max-brightness = <255>;
		};

		ledpwm3 {
			label = "PWM<C>";
			pwms = <&pwm4 0 50000>;
			max-brightness = <255>;
		};
#endif
		ledpwm4 {
			label = "PWM<D>";
			pwms = <&pwm2 0 50000>;
			max-brightness = <255>;
		};
	};

	regulators {
		reg_usb_host_vbus: usb_host_vbus {
			status = "okay";
		};
	};
};

&backlight {
#if 0 /* PWM polarity: if 1 is brightest */
#if 0 /* Fusion 7 needs 10kHz PWM frequency */
	pwms = <&pwm3 0 100000>;
#endif
	brightness-levels = <0 4 8 16 32 64 128 255>;
	default-brightness-level = <6>;
#else /* PWM plarity: if 0 is brightest */
	brightness-levels = <0 127 191 223 239 247 251 255>;
	default-brightness-level = <1>;
#endif
	status = "okay";
};

/ {
   clocks {
       /* fixed crystal dedicated to mpc251x */
       clk16m: clk@1 {
           compatible = "fixed-clock";
           reg=<1>;
           #clock-cells = <0>;
           clock-frequency = <16000000>;
           clock-output-names = "clk16m";
       };
   };
};

/* Colibri SPI */
&ecspi4 {
	status = "okay";

   #if 0
	spidev0: spidev@1 {
		compatible = "spidev";
		reg = <0>;
		spi-max-frequency = <23000000>;
	};
#else
   can0: can@1 {
       compatible = "microchip,mcp2515";
       reg = <0>;
       clocks = <&clk16m>;
       interrupt-parent = <&gpio3>;
       interrupts = <27 0x2>;
       spi-max-frequency = <10000000>;
   };
   #endif

};

&flexcan1 {
       status = "okay";
};

&hdmi_audio {
	status = "okay";
};

&hdmi_core {
	status = "okay";
};

&hdmi_video {
	status = "okay";
};

/*
 * I2C: I2C3_SDA/SCL on SODIMM 194/196 (e.g. RTC on carrier
 * board)
 */
&i2c3 {
	status = "okay";
#ifdef PCAP /* not standard pinout, disable PWM<B>, PWM<C> */
	pcap@10 {
		/* TouchRevolution Fusion 7 and 10 multi-touch controller */
		compatible = "touchrevolution,fusion-f0710a";
		reg = <0x10>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_pcap_1>;
		gpios = <&gpio1  9 0 /* SODIMM 28, Pen down interrupt */
			 &gpio2 10 0 /* SODIMM 30, Reset */
			>;
	};
#endif

	tsc@24 {
		compatible = "cy,cyttsp5_i2c_adapter";
		reg = <0x24>;

		/* SODIMM 103, Pen down interrupt  high-to-low edge triggered */
		cy,adapter_id = "cyttsp5_i2c_adapter";

		cy,core {
			cy,name = "cyttsp5_core";
			cy,id = "main_ttsp_core";

			cy,irq_gpio = <52>;
			cy,rst_gpio = <53>;
			cy,hid_desc_register = <1>;
			/* CY_CORE_FLAG_WAKE_ON_GESTURE */
			cy,flags = <1>;
			cy,btn_keys = <172 /* KEY_HOMEPAGE */
					   /* previously was KEY_HOME, new Android versions use KEY_HOMEPAGE */
				       139 /* KEY_MENU */
				       158 /* KEY_BACK */
				       217 /* KEY_SEARCH */
				       114 /* KEY_VOLUMEDOWN */
				       115 /* KEY_VOLUMEUP */
				       212 /* KEY_CAMERA */
				       116>; /* KEY_POWER */
			cy,btn_keys-tag = <0>;

			cy,mt {
				cy,name = "cyttsp5_mt";

				cy,inp_dev_name = "cyttsp5_mt";
				/* CY_MT_FLAG_FLIP | CY_MT_FLAG_INV_X | CY_MT_FLAG_INV_Y */
				cy,flags = <0x38>;
				cy,abs =
					/* ABS_MT_POSITION_X, CY_ABS_MIN_X, CY_ABS_MAX_X, 0, 0 */
					<0x36 0 800 0 0
					/* ABS_MT_POSITION_Y, CY_ABS_MIN_Y, CY_ABS_MAX_Y, 0, 0 */
					0x35 0 480 0 0
					/* ABS_MT_PRESSURE, CY_ABS_MIN_P, CY_ABS_MAX_P, 0, 0 */
					0x3a 0 255 0 0
					/* CY_IGNORE_VALUE, CY_ABS_MIN_W, CY_ABS_MAX_W, 0, 0 */
					0xffff 0 255 0 0
					/* ABS_MT_TRACKING_ID, CY_ABS_MIN_T, CY_ABS_MAX_T, 0, 0 */
					0x39 0 15 0 0
					/* ABS_MT_TOUCH_MAJOR, 0, 255, 0, 0 */
					0x30 0 255 0 0
					/* ABS_MT_TOUCH_MINOR, 0, 255, 0, 0 */
					0x31 0 255 0 0
					/* ABS_MT_ORIENTATION, -128, 127, 0, 0 */
					0x34 0xfffffed7 127 0 0
					/* ABS_MT_TOOL_TYPE, 0, MT_TOOL_MAX, 0, 0 */
					0x37 0 1 0 0>;

				cy,vkeys_x = <800>;
				cy,vkeys_y = <480>;

				cy,virtual_keys = /* KeyCode CenterX CenterY Width Height */
					/* KEY_BACK */
					<158 1360 90 160 180
					/* KEY_MENU */
					139 1360 270 160 180
					/* KEY_HOMEPAGE */
					172 1360 450 160 180
					/* KEY SEARCH */
					217 1360 630 160 180>;
			};

			cy,btn {
				cy,name = "cyttsp5_btn";

				cy,inp_dev_name = "cyttsp5_btn";
			};

			cy,proximity {
				cy,name = "cyttsp5_proximity";

				cy,inp_dev_name = "cyttsp5_proximity";
				cy,abs =
					/* ABS_DISTANCE, CY_PROXIMITY_MIN_VAL, CY_PROXIMITY_MAX_VAL, 0, 0 */
					<0x19 0 1 0 0>;
			};
		};

	};

	/* M41T0M6 real time clock on carrier board */
	rtc_i2c: rtc@68 {
		compatible = "st,m41t00";
		reg = <0x68>;
	};
};

/*
 * DDC_I2C: I2C2_SDA/SCL on extension connector pin 15/16
 */
&i2cddc {
	status = "okay";

	hdmi: edid@50 {
		compatible = "fsl,imx6-hdmi-i2c";
		reg = <0x50>;
	};
};

&iomuxc {
	/*
	 * Mux all pins which are unused to be GPIOs
	 * so they are ready for export to user space
	 */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_weim_gpio_1 &pinctrl_weim_gpio_2
	             &pinctrl_weim_gpio_3 &pinctrl_weim_gpio_4
	             &pinctrl_weim_gpio_5 &pinctrl_weim_gpio_6
	             &pinctrl_csi_gpio_1
	             &pinctrl_gpio_1
	             &pinctrl_usbh_oc_1 &pinctrl_usbc_id_1>;

	gpio {
		pinctrl_pcap_1: pcap-1 {
			fsl,pins = <
				MX6QDL_PAD_GPIO_9__GPIO1_IO09	PAD_CTRL_HYS_PD /* SODIMM 28 */
				MX6QDL_PAD_SD4_DAT2__GPIO2_IO10	PAD_CTRL_HYS_PD /* SODIMM 30 */
			>;
		};
	};

	touch {
		pinctrl_cyttsp5_int: cyttsp5_intgrp {
			fsl,pins = <
				MX6QDL_PAD_EIM_A17__GPIO2_IO21		PAD_CTRL_HYS_PD /* Colibri x16-16 */
				MX6QDL_PAD_EIM_A18__GPIO2_IO20		PAD_CTRL_HYS_PD /* Colibri x16-15 */
			>;
		};
	};
};

&lcd {
	status = "okay";
};

&mxcfb1 {
	status = "okay";
};

&mxcfb2 {
	status = "okay";
};

#ifndef PCAP
&pwm1 {
	status = "okay";
};
#endif

&pwm2 {
	status = "okay";
};

&pwm3 {
	status = "okay";
};

#ifndef PCAP
&pwm4 {
	status = "okay";
};
#endif

&sound_hdmi {
	status = "okay";
};

&uart1 {
	status = "okay";
};

&uart2 {
	status = "okay";
#if 0
	linux,rs485-enabled-at-boot-time;
#endif
};

&uart3 {
	status = "okay";
};

&usbh1 {
	status = "okay";
};

&usbotg {
	status = "okay";
	extcon = <&extcon_usbc_det>;
};

/* MMC */
&usdhc1 {
	status = "okay";
};

&weim {
	status = "okay";
	/* weim memory map: 32MB on CS0, 32MB on CS1, 32MB on CS2 */
	ranges = <0 0 0x08000000 0x02000000
	          1 0 0x0a000000 0x02000000
	          2 0 0x0c000000 0x02000000>;
	/* SRAM on CS0 */
	sram@0,0 {
		compatible = "cypress,cy7c1019dv33-10zsxi, mtd-ram";
		reg = <0 0 0x00010000>;
		#address-cells = <1>;
		#size-cells = <1>;
		bank-width = <2>;
		fsl,weim-cs-timing = <0x00010081 0x00000000 0x04000000
				0x00000000 0x04000040 0x00000000>;
	};
	/* SRAM on CS1 */
	sram@1,0 {
		compatible = "cypress,cy7c1019dv33-10zsxi, mtd-ram";
		reg = <1 0 0x00010000>;
		#address-cells = <1>;
		#size-cells = <1>;
		bank-width = <2>;
		fsl,weim-cs-timing = <0x00010081 0x00000000 0x04000000
				0x00000000 0x04000040 0x00000000>;
	};
};
